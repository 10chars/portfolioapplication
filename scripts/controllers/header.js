'use strict';

/**
 * @ngdoc function
 * @author Luke Helg
 * @name portfolioApp.controller: HeaderCtrl
 * @description
 * # HeaderCtrl
 * Controller of the portfolioApp
 */
angular.module('portfolioApp')
  .controller('HeaderCtrl', function($scope, $location) { 
  $scope.isActive = function(path) {
    if ($location.path() === path) {
      return true;
    } else {
      return false;   
  }
}
});