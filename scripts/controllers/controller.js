'use strict';

/**
 * @ngdoc function
 * @author Luke Helg
 * @name portfolioApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the portfolioApp
 */

angular.module('portfolioApp')
  .controller('Ctrl', function($scope, English, Japanese, $document, SavedData ,$rootScope, $routeParams, $location, $http) {    
    
     $scope.selectedLanguage = SavedData.getLang();
     $rootScope.currentSiteID = $routeParams.websiteID;
        
    if($scope.selectedLanguage  === 'en'){
    	$rootScope.sites = English.getSitesEN();
    	$rootScope.information = English.getInfoEN();
    	$rootScope.welcome = English.getWelcomeEN();	
      $rootScope.contactStrings = English.getContactEN();
      $rootScope.about = English.getAboutEN();
      $rootScope.trait = English.getTraitEN();
      $rootScope.currentSite = $rootScope.sites[$scope.currentSiteID];
      $rootScope.isEN = true;
      $rootScope.isJP = false;
      $rootScope.portfolioString = "View Portfolio";
      
    }else{
    	$rootScope.sites = Japanese.getSitesJP();
    	$rootScope.information = Japanese.getInfoJP();
    	$rootScope.welcome = Japanese.getWelcomeJP();
      $rootScope.contactStrings = Japanese.getContactJP();
      $rootScope.about = Japanese.getAboutJP();
      $rootScope.trait = Japanese.getTraitJP();
      $rootScope.currentSite = $rootScope.sites[$scope.currentSiteID];
      $rootScope.isJP = true;
      $rootScope.isEN = false;
      $rootScope.portfolioString = "ポートフォリオ";

    }

    $scope.switchEnglish = function(){     
    	$rootScope.sites = English.getSitesEN();
    	$rootScope.information = English.getInfoEN();
      $rootScope.welcome = English.getWelcomeEN();  
    	$rootScope.isEN = true;
      $rootScope.isJP = false;
      SavedData.setLang('en');
      $scope.langSet = true;
      $rootScope.contactStrings = English.getContactEN();
      $rootScope.trait = English.getTraitEN();
      $rootScope.about = English.getAboutEN();
      $rootScope.currentSite = $rootScope.sites[$scope.currentSiteID];
      $rootScope.portfolioString = "Portfolio";
    }

    $scope.switchJapanese = function(){
    	$rootScope.sites = Japanese.getSitesJP();
    	$rootScope.information = Japanese.getInfoJP();
      $rootScope.welcome = Japanese.getWelcomeJP();
      $rootScope.isEN = false;
      $rootScope.isJP = true;
    	SavedData.setLang('jp');
      $scope.langSet = true;
      $rootScope.contactStrings = Japanese.getContactJP();
      $rootScope.trait = Japanese.getTraitJP();
      $rootScope.about = Japanese.getAboutJP();
      $rootScope.currentSite = $rootScope.sites[$scope.currentSiteID];
      $rootScope.portfolioString = "Portfolio";

    	
    }
    
    $scope.result = 'hidden';
    $rootScope.resultMessage;
    $rootScope.formData = {}; 
    $scope.submitButtonDisabled = false;
    $scope.submitted = false; 
    $scope.submit = function(contactform) {
         $scope.submitted = true;
        $scope.submitButtonDisabled = true;
        if (contactform.$valid) {
            $http({
                method  : 'POST',
                url     : 'contact-form.php',
                data    : $.param($rootScope.formData),  
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
            }).success(function(data){
                console.log(data);
                if (data.success) {
                    $scope.submitButtonDisabled = true;
                    $scope.resultMessage = data.message;
                    $scope.result='bg-success';
                } else {
                    $scope.submitButtonDisabled = false;
                    $scope.resultMessage = data.message;
                    $scope.result='bg-danger';
                }
            });
        } else {
            $scope.submitButtonDisabled = false;
            $scope.resultMessage = 'Failed :( Please fill out all the fields.';
            $scope.result='bg-danger';
        }
    }

});


