'use strict';

/**
 * @ngdoc overview
 * @name portfolioApp
 * @description
 * # portfolioApp
 *
 * Main module of the application.
 */
angular
  .module('portfolioApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'MyData',
    'duScroll'

  ])
  .config(function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'Ctrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'Ctrl'
      })
      .when('/contact', {
        templateUrl: 'views/contact.html',
        controller: 'Ctrl'
      })
      .when('/portfolio', {
        templateUrl: 'views/portfolio.html',
        controller: 'Ctrl'
      })
      .when('/portfolio/:websiteID', {
        templateUrl: 'views/portfolio.html',
        controller: 'Ctrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
