'use strict';

/**
 * @ngdoc function
 * @author Luke Helg
 * @name portfolioApp.controller: Custom Directives
 * @description
 * # directive
 * Custom directives for portfolioApp
 */
angular.module('portfolioApp')
  .directive('resizable', function($window) {
    return function($scope) {
    $scope.initializeWindowSize = function() {
      console.log('Sent Border width:  ' + $window.innerWidth/2);
      return $scope.windowWidth = $window.innerWidth/2;
    };
    $scope.initializeWindowSize();
    return angular.element($window).bind('resize', function() {
      $scope.initializeWindowSize();
      return $scope.$apply();
    });
  };
})

  .directive('dynpad', function($window) {
    return function($scope) {
    $scope.initializeWindowHeight = function() {
      return $scope.dynamic = Math.round($window.innerHeight * 0.2);
    };
    $scope.initializeWindowHeight();
    return angular.element($window).bind('resize', function() {
      $scope.initializeWindowHeight();
      return $scope.$apply();
    });
  };

});

