<?php
/**
 * User: Lukey
 */
    echo 'allo sonny!';
    if(isset($_POST['email'])) {

        // EDIT THE 2 LINES BELOW AS REQUIRED
        $email_to = "luke.helg@gmail.com";
        $email_subject = "Message from PIROGRAPH";

        function died($error) {
            // Not working! Using JS instead for now
            echo "We are very sorry, but there were error(s) found with the form you submitted. ";
            echo "These errors appear below.<br /><br />";
            echo $error."<br /><br />";
            echo "Please go back and fix these errors.<br /><br />";
            die();
        }

        // validation expected data exists
        if(!empty($_POST['first_name']) ||
            !isset($_POST['last_name']) ||
            !isset($_POST['email'])) {
            died('We are sorry, but there appears to be a problem with the form you submitted.');
        }

        $first_name = $_POST['first_name']; // required
        $last_name = $_POST['last_name']; // required
        $email_from = $_POST['email']; // required
        $people = $_POST['people'];
        $day = $_POST['day'];
        $time = $_POST['time'];

        if(isset($_POST['phone'])){
            $phone = $_POST['phone'];
        }else $phone = 'N/A';

        $error_message = "";
        $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
        if(!preg_match($email_exp,$email_from)) {
            $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
        }
        $string_exp = "/^[A-Za-z .'-]+$/";
        if(!preg_match($string_exp,$first_name)|| $first_name = null)  {
            $error_message .= 'The First Name you entered does not appear to be valid.<br />';
        }
        if(!preg_match($string_exp,$last_name)) {
            $error_message .= 'The Last Name you entered does not appear to be valid.<br />';
        }
        if(strlen($error_message) > 0) {
            died($error_message);
        }
        $email_message = "ENQUIRY from:".$first_name."\n\n";

        function clean_string($string) {
            $bad = array("content-type","bcc:","to:","cc:","href");
            return str_replace($bad,"",$string);
        }

        $email_message .= "First Name: ".clean_string($first_name)."\n";
        $email_message .= "Last Name: ".clean_string($last_name)."\n";
        $email_message .= "Email: ".clean_string($email_from)."\n";
        $email_message .= "Phone: ".clean_string($phone)."\n";
        $email_message .= "People: ".clean_string($people)."\n";
        $email_message .= "Day: ".clean_string($day)."\n";
        $email_message .= "Time: ".clean_string($time)."\n";

        // create email headers
        $headers = 'From: '.$email_from."\r\n".
            'Reply-To: '.$email_from."\r\n" .
            'X-Mailer: PHP/' . phpversion();
        @mail($email_to, $email_subject, $email_message, $headers);
        sleep(2);
        echo "<meta http-equiv='refresh' content=\"0; url=http://www.google.com\">";
    }

?>
